import json
import os


def openJSON(path):
    return json.loads(open(path, "r").read())

def saveJSON(path, obj):
    f = open(path, "w")
    raw = json.dumps(obj, indent=4)
    f.write(raw)
    f.close()


class Database:
    def __init__(self, file):
        self.database = {
            "config": {
                "poll_delay": 5,
                "telegram_bot_token": "token from @BotFather",
                "telegram_api_id": 123456,
                "telegram_api_hash": "get app_id and app_hash from https://my.telegram.org/apps",
                "admin": 0,
                "instance": "",
                "encryption_key": None
            },
            "users": {}
        }
        self.file = file

    def load(self):
        if os.path.exists(self.file):
            self.database = openJSON(self.file)
        else:
            self.save()

    def save(self):
        saveJSON(self.file, self.database)

    def get_conf(self, key):
        return self.database["config"][key]

    def update_conf(self, key, value):
        self.database["config"][key] = value

    def get_user(self, username, key):
        return self.database["users"][str(username)][key]

    def update_user(self, username, key, value):
        self.database["users"][str(username)][key] = value

    def is_admin(self, id):
        return self.database["config"]["admin"] == id

    def is_user(self, id):
        return str(id) in self.database["users"]