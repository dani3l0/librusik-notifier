# Librusik Notifier

**Project abandoned due to unmaintainable code**

**As of 1 Feb 2023 Librus has disabled notification API endpoint. It's over.**

A free way to get notifications from Librus on Telegram.


### Installation

**1. Install required Python modules:**
```
pip install -r requirements.txt
```

**2. Download Librusik Notifier**

```
git clone https://gitlab.com/dani3l0/librusik-notifier LibrusikNotifier
cd LibrusikNotifier
```

**3. Download Librusik**
As Notifier uses Librusik's internal APIs to fetch data from Librus.

```
git clone https://gitlab.com/dani3l0/librusik
```

**4. Run the bot!**

On first run an initial setup will be shown where you will have to provide information nesessary to run the bot.

```
python3 notifier.py
```

If you see `Bot started`, bot is up and configured properly.

### Bot command list

Go to BotFather, select your bot and click `Edit Commands`. Paste this:

```
status - Get current notifications on Synergia
me - Show information about connected account
settings - Bot settings
configure - Configure / change Synergia account
logout - Log out
about - About bot
```

This will make your bot more friendly to other users.

### Admin-ing

When each person logs in, bot will send you a prompt to accept the new person.

You can list & manage your users by typing `/users`.

Moreover, you can use `/broadcast <message>` to send a message for all registered users!
