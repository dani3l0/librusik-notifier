import asyncio
import base64
from librusik.lib.api import Librus
from telethon import TelegramClient, events
import os
import hashlib
from cryptography.fernet import Fernet
import json
from datetime import datetime
import time
from lib.utils import *

database = Database("notifier.json")
database.load()

if not database.get_conf("encryption_key"):
	print("Hello! It seems Librusik Notifier is running for the first time.")
	print("Let's configure things.\n")
	print("1. Connect to a Telegram bot.")
	print("  Please login to https://my.telegram.org/apps and go to API development tools. Paste requested values below.")
	_api_id = input("   a) App API id: ")
	_api_hash = input("   b) App API hash: ")
	print("  Now, go to @BotFather and create a new bot which will send you notifications.")
	_bot_token = input("   c) Bot token: ")
	_instance_uri = input("2. Your Librusik instance URL (full URL) (you can leave this empty): ")
	print("3. Admin setup.")
	print("  Go and send a message to your just created bot. First message sender will be automatically marked as admin.")
	fernet = base64.urlsafe_b64encode(os.urandom(32)).decode()
	database.update_conf("telegram_api_id", _api_id)
	database.update_conf("telegram_api_hash", _api_hash)
	database.update_conf("telegram_bot_token", _bot_token)
	database.update_conf("instance", _instance_uri)
	database.update_conf("encryption_key", fernet)
	database.save()
	print("Yay! You've successfully configured Notifier. Don't forget to connect your Synergia account!")


bot = TelegramClient(
	'notifier',
	database.get_conf("telegram_api_id"),
	database.get_conf("telegram_api_hash")
).start(bot_token = database.get_conf("telegram_bot_token"))

admin = database.get_conf("admin")

##############################################################################

frt = Fernet(database.get_conf("encryption_key").encode())
tempbase = {}
messages_session = {}
notworkingaccounts = []

# More debugging
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


print("Bot started")

async def sendMessage(user_id, msg):
	try:
		user = await bot.get_entity(int(user_id))
		return await bot.send_message(user, msg)
	except:
		return None

def setworking(user, state = True):
	global notworkingaccounts
	if state:
		if user in notworkingaccounts:
			notworkingaccounts.remove(str(user))
		return
	else:
		if user not in notworkingaccounts:
			notworkingaccounts.append(str(user))
def updatedb():
	database.save()

def encrypt(what):
	coded = frt.encrypt(what.encode())
	return coded.decode()
def decrypt(what):
	coded = frt.decrypt(what.encode())
	return coded.decode()

async def auth(user, passwd):
	librus = Librus()
	return await librus.mktoken(user, passwd)

def bool2str(boll):
	if boll:
		return "✅"
	return "❌"

def access(userid, state = None):
	try:
		global database
		if state != None:
			database.database["users"][str(userid)]["allowed"] = state
		updatedb()
		return database.database["users"][str(userid)]["allowed"]
	except:
		return False;

def readdb(user_id, what):
	try:
		return database.database["users"][str(user_id)][what]
	except:
		return None

def listusers():
	liste = "**Bot users:**"
	if len(database.database["users"]) > 0:
		for user in database.database["users"]:
			if user in notworkingaccounts:
				liste += "\n\n{}🔑 [{} {}](tg://user?id={uid})\n/allow{uid}`   `/deny{uid}".format(bool2str(readdb(user, "allowed")), database.database["users"][str(user)]["first_name"], database.database["users"][str(user)]["last_name"], uid = user)
			else:
				liste += "\n\n{} [{} {}](tg://user?id={uid})\n/allow{uid}`   `/deny{uid}".format(bool2str(readdb(user, "allowed")), database.database["users"][str(user)]["first_name"], database.database["users"][str(user)]["last_name"], uid = user)
	else:
		liste += "\n\n__Nobody uses this bot...__"
	return liste

def getsetting(userid, item):
	return database.database["users"][str(userid)][item]

async def adduser(userid, username, passwd):
	global database
	for user in database.database["users"]:
		if username == database.database["users"][user]["username"]:
			return False
	librus = Librus()
	if (await librus.mktoken(username, passwd)):
		me = await librus.get_me()
		database.database["users"][str(userid)] = {
			"first_name": me["FirstName"],
			"last_name": me["LastName"],
			"username": username,
			"passwd": encrypt(passwd),
			"n_global": True,
			"n_minimal": False,
			"msg_preview": False,
			"allowed": False
		}
		updatedb()
		return True
	return None

def deluser(userid):
	global database
	if userid in database.database["users"]:
		del database.database["users"][str(userid)]
		updatedb()

for user in database.database["users"]:
	librus = Librus()
	tempbase[user] = {
		"Messages": [],
		"Attendances": [],
		"FreeDays": [],
		"TeacherFreeDays": [],
		"ParentTeacherConferences": [],
		"Grades": [],
		"HomeWorks": [],
		"Notices": [],
		"session": librus
	}
	messages_session[user] = None

def cacheit(user, data):
	global tempbase
	print(data)
	tempbase[str(user)]["Messages"] = []
	for xd in data["Messages"]:
		tempbase[str(user)]["Messages"].append(xd["Id"])
	tempbase[str(user)]["Attendances"] = []
	for xd in data["Attendances"]:
		tempbase[str(user)]["Attendances"].append(xd["Id"])
	tempbase[str(user)]["FreeDays"] = []
	for xd in data["CalendarItems"]["SchoolFreeDays"]:
		tempbase[str(user)]["FreeDays"].append(xd["Id"])
	tempbase[str(user)]["TeacherFreeDays"] = []
	for xd in data["CalendarItems"]["TeacherFreeDays"]:
		tempbase[str(user)]["TeacherFreeDays"].append(xd["Id"])
	tempbase[str(user)]["ParentTeacherConferences"] = []
	for xd in data["CalendarItems"]["ParentTeacherConferences"]:
		tempbase[str(user)]["ParentTeacherConferences"].append(xd["Id"])
	tempbase[str(user)]["Grades"] = []
	for xd in data["Grades"]:
		tempbase[str(user)]["Grades"].append(xd["Id"])
	tempbase[str(user)]["Notices"] = []
	for xd in data["SchoolNotices"]:
		tempbase[str(user)]["Notices"].append(xd["Id"])
	tempbase[str(user)]["HomeWorks"] = []
	for xd in data["CalendarItems"]["HomeWorks"]:
		tempbase[str(user)]["HomeWorks"].append(xd["Id"])


async def mksession(user):
	librus = Librus()
	if await librus.mktoken(database.database["users"][str(user)]["username"], decrypt(database.database["users"][str(user)]["passwd"])):
		tempbase[str(user)]["session"] = librus
		return True
	else:
		return None

async def refreshtemps(user):
	global tempbase
	global notworkingaccounts
	if user not in tempbase:
		librus = Librus()
		tempbase[user] = {
			"Messages": [],
			"Attendances": [],
			"FreeDays": [],
			"TeacherFreeDays": [],
			"ParentTeacherConferences": [],
			"Grades": [],
			"HomeWorks": [],
			"Notices": [],
			"session": librus
		}
		if not (await mksession(user)):
			setworking(user, False)
		else:
			data = await tempbase[str(user)]["session"].get_notifications()
			if data:
				cacheit(user, data)
				setworking(user, True)
			else:
				setworking(user, False)

async def check_updates():
	global notworkingaccounts
	global tempbase
	global messages_session
	for user in database.database["users"]:
		if not (await mksession(user)):
			setworking(user, False)
		else:
			data = await tempbase[str(user)]["session"].get_notifications()
			if data:
				cacheit(user, data)
				setworking(user, True)
			else:
				setworking(user, False)
	while True:
		for user in database.database["users"]:
			await refreshtemps(user)
			if getsetting(user, "n_global") and database.database["users"][user]["allowed"]:
				data = await tempbase[str(user)]["session"].get_notifications()
				print(await tempbase[str(user)]["session"].get_data("WhatsNew"))
				print(data)
				if data == None:
					await mksession(user)
					data = await tempbase[str(user)]["session"].get_notifications()
				if not data:
					setworking(user, False)
				else:
					setworking(user, True)
					for item in data["Messages"]:
						if item["Id"] not in tempbase[user]["Messages"]:
							try:
								if not getsetting(user, "msg_preview"):
									raise KeyError
								mesg = await tempbase[str(user)]["session"].get_message("1-5-{}-f0".format(str(item["Id"])))
								attachments = ""
								numb = len(mesg["attachments"])
								if numb:
									addon = ""
									if numb != 1:
										addon = "s"
									attachments = " (with {} attachment{})".format(numb, addon)
								sentby = mesg["from"]
								subj = mesg["subject"]
								mesg = mesg["content"]
								warn = ""
								if len(mesg) > 1000:
									nm = len(mesg) - 1000
									warn = "\n\n[Showing `1000/{}` characters]".format(nm)
								await sendMessage(int(user), "**New message!{}**\nfrom {}\n\n**{}**\n__{}__{}".format(attachments, sentby, subj, mesg[:1000], warn))
							except:
								await sendMessage(int(user), "**New message!**")
					for item in data["SchoolNotices"]:
						if item["Id"] not in tempbase[user]["Notices"]:
							await sendMessage(int(user), "**New school notice!**")

					for item in data["Grades"]:
						if item["Id"] not in tempbase[user]["Grades"]:
							try:
								if getsetting(user, "n_minimal"):
									raise NotThis
								item = (await tempbase[str(user)]["session"].curl(item["Url"]))["text"]["Grade"]
								grade = item["Grade"]
								categ = (await tempbase[str(user)]["session"].curl(item["Category"]["Url"]))["text"]["Category"]
								subject = (await tempbase[str(user)]["session"].curl(item["Subject"]["Url"]))["text"]["Subject"]["Name"]
								if "Comments" in item:
									comment = (await tempbase[str(user)]["session"].curl(item["Comments"][0]["Url"]))["text"]["Comment"]["Text"]
								else:
									comment = "No comment"
								if "Weight" in categ:
									weight = categ["Weight"]
								else:
									weight = "No weight"
								await sendMessage(int(user), "**New grade!**\n\n`Subject:  `{}\n`Grade:    `{}\n`Type:     `{}\n`Weight:   `{}\n\n`Comment:  `__{}__\n".format(subject, grade, categ["Name"], weight, comment))
							except:
								await sendMessage(int(user), "**New grade detected!**")

					for item in data["Attendances"]:
						if item["Id"] not in tempbase[user]["Attendances"]:
							try:
								if getsetting(user, "n_minimal"):
									raise NotThis
								item = (await tempbase[str(user)]["session"].curl(item["Url"]))["text"]["Attendance"]
								subject = (await tempbase[str(user)]["session"].curl(item["Lesson"]["Url"]))["text"]["Lesson"]["Subject"]["Url"]
								subject = (await tempbase[str(user)]["session"].curl(subject))["text"]["Subject"]["Name"]
								categ = (await tempbase[str(user)]["session"].curl(item["Type"]["Url"]))["text"]["Type"]["Name"]
								addedby = (await tempbase[str(user)]["session"].curl(item["AddedBy"]["Url"]))["text"]["User"]
								addedby = "{} {}".format(addedby["FirstName"], addedby["LastName"])
								date = "{}, lesson {}".format(item["Date"], item["LessonNo"])
								await sendMessage(int(user), "**New absence!**\n\n`Date:     `{}\n`Subject:  `{}\n`Type:     `{}\n`Added by: `{}".format(date, subject, categ, addedby))
							except:
								await sendMessage(int(user), "**New absence detected!**")

					for item in data["CalendarItems"]["HomeWorks"]:
						if item["Id"] not in tempbase[user]["HomeWorks"]:
							try:
								if getsetting(user, "n_minimal"):
									raise NotThis
								item = (await tempbase[str(user)]["session"].curl(item["Url"]))["text"]["HomeWork"]
								categ = (await tempbase[str(user)]["session"].curl(item["Category"]["Url"]))["text"]["Category"]["Name"]
								subject = (await tempbase[str(user)]["session"].curl(item["Subject"]["Url"]))["text"]["Subject"]["Name"]
								date = item["Date"]
								timex = "{} - {}".format(item["TimeFrom"][:-3], item["TimeTo"][:-3])
								if "Content" in item:
									comment = item["Content"]
								else:
									comment = "No comment"
								await sendMessage(int(user), "**New exam!**\n\n`Type:     `{}\n`Subject:  `{}\n`Date:     `{}\n`Time:     `{}\n\n`Comment:  `\n__{}__".format(categ, subject, date, timex, comment))
							except:
								await sendMessage(int(user), "**New exam detected!**")

					for item in data["CalendarItems"]["SchoolFreeDays"]:
						if item["Id"] not in tempbase[user]["FreeDays"]:
							try:
								if getsetting(user, "n_minimal"):
									raise NotThis
								item = (await tempbase[str(user)]["session"].curl(item["Url"]))["text"]["SchoolFreeDay"]
								await sendMessage(int(user), "**New free day(s)!**\n\n`Name:     `{}\n`From:     `{}\n`To:       `{}".format(item["Name"], item["DateFrom"], item["DateTo"]))
							except:
								await sendMessage(int(user), "**New free day(s) detected!**")

					for item in data["CalendarItems"]["TeacherFreeDays"]:
						if item["Id"] not in tempbase[user]["TeacherFreeDays"]:
							try:
								if getsetting(user, "n_minimal"):
									raise NotThis
								item = (await tempbase[str(user)]["session"].curl(item["Url"]))["text"]["TeacherFreeDay"]
								if "TimeFrom" in item and "TimeTo" in item:
									item["DateFrom"] += ", {}".format(item["TimeFrom"][:-3])
									item["DateTo"] += ", {}".format(item["TimeTo"][:-3])
								teacher = (await tempbase[str(user)]["session"].curl(item["Teacher"]["Url"]))["text"]["User"]
								teacher = "{} {}". format(teacher["FirstName"], teacher["LastName"])
								await sendMessage(int(user), "**New teacher absences!**\n\n`Teacher:  `{}\n`From:     `{}\n`To:       `{}".format(teacher, item["DateFrom"], item["DateTo"]))
							except:
								await sendMessage(int(user), "**New teacher absences!**")

					for item in data["CalendarItems"]["ParentTeacherConferences"]:
						if item["Id"] not in tempbase[user]["ParentTeacherConferences"]:
							try:
								if getsetting(user, "n_minimal"):
									raise NotThis
								item = (await tempbase[str(user)]["session"].curl(item["Url"]))["text"]["ParentTeacherConference"]
								await sendMessage(int(user), "**New parent-teacher conference...**\n\n`Name:     `{}\n`Date:     `{}, {}\n`Room:     `{}\n\n`Topic:    `\n__{}__".format(item["Name"], item["Date"], item["Time"][:-3], item["Room"], item["Topic"]))
							except:
								await sendMessage(int(user), "**Detecded new parent-teacher conference...**")
					cacheit(user, data)
		tajm = datetime.now()
		if tajm.hour == 23:
			await asyncio.sleep(28800)
		await asyncio.sleep(240)



asyncio.gather(check_updates())

def mksettingsui(userid):
	return "**Librusik Notifier Settings**\n`--------------------------------`\n\n{}` `**Notifications**\n/n` `ON/OFF **all** notifications\n\n{}` `**Minimal mode**\n/nm` `Notify without any event details\n\n{}` `**Message preview**\n/np` `All messages on Synergia will be marked as read".format(
		bool2str(database.database["users"][str(userid)]["n_global"]),
		bool2str(database.database["users"][str(userid)]["n_minimal"]),
		bool2str(database.database["users"][str(userid)]["msg_preview"])
	)

@bot.on(events.NewMessage)
async def main(event):
	global database
	msg = event.message.text

	if not database.get_conf("admin"):
		database.update_conf("admin", event.chat_id)
		database.save()
		await sendMessage(event.chat_id, "You're admin!")

	if msg == "/settings" and access(event.chat_id):
		await sendMessage(event.chat_id, mksettingsui(event.chat_id))

	elif msg == "/me" and database.is_user(event.chat_id):
		if str(event.chat_id) in notworkingaccounts:
			wrk = "⚠️` `__Can't fetch data from Synergia__"
		else:
			wrk = "✅` `__Everything is working properly__"
		if database.database["users"][str(event.chat_id)]["allowed"]:
			alw = "✅` `__Account confirmed__"
		else:
			alw = "❌` `__Account not confirmed__"
		await sendMessage(event.chat_id, "**My account**\n`--------------------------------`\n\n👤` `{} {}\n📌` ``{}`\n\n{}\n{}".format(
			database.database["users"][str(event.chat_id)]["first_name"],
			database.database["users"][str(event.chat_id)]["last_name"],
			database.database["users"][str(event.chat_id)]["username"],
			alw,
			wrk
		))

	elif msg.startswith("/n") and access(event.chat_id):
		if msg == "/n":
			database.database["users"][str(event.chat_id)]["n_global"] = not database.database["users"][str(event.chat_id)]["n_global"]
		elif msg == "/nm":
			database.database["users"][str(event.chat_id)]["n_minimal"] = not database.database["users"][str(event.chat_id)]["n_minimal"]
		elif msg == "/np":
			database.database["users"][str(event.chat_id)]["msg_preview"] = not database.database["users"][str(event.chat_id)]["msg_preview"]
		if msg in ["/n", "/nm", "/np"]:
			updatedb()
			await sendMessage(event.chat_id, mksettingsui(event.chat_id))

	elif msg.startswith("/configure"):
		words = msg.split(" ")
		if len(words) >= 3:
			startby = len(words[0] + words[1]) + 2
			password = msg[startby:]
			added = await adduser(event.chat_id, words[1], password)
			if added:
				setworking(event.chat_id, True)
				await sendMessage(admin, "[{} {}](tg://user?id={uid}) wants to use this bot.\n/allow{uid}`   `/deny{uid}".format(database.database["users"][str(event.chat_id)]["first_name"], database.database["users"][str(event.chat_id)]["last_name"], uid = event.chat_id))
				await sendMessage(event.chat_id, "Access request has been sent to bot owner.")
			elif added == None:
				await sendMessage(event.chat_id, "You have provided wrong credentials. Please try again.")
			else:
				if str(event.chat_id) in database.database["users"]:
					if await auth(words[1], password):
						database.database["users"][str(event.chat_id)]["username"] = words[1]
						database.database["users"][str(event.chat_id)]["passwd"] = encrypt(password)
						updatedb()
						await sendMessage(event.chat_id, "Credentials updated successfully.")
					else:
						await sendMessage(event.chat_id, "You have provided wrong credentials. Please try again.")
				else:
					await sendMessage(event.chat_id, "Sorry, but another Telegram user is already using this account.")
		else:
			await sendMessage(event.chat_id, "**Wrong input.**\nMake sure you provided **both** login and password for Synergia, for example:\n\n`/configure 12345678u mypass456`")

	elif event.chat.id == admin and msg.startswith("/broadcast"):
		if msg == "/broadcast":
			return await sendMessage(admin, "Where is the message?")
		i = 0
		try:
			text = msg[11:]
			for u in database.database["users"]:
				if int(u) != admin:
					try:
						await sendMessage(int(u), "**Message from bot owner:**\n{}".format(text))
						i += 1
					except:
						pass
		except:
			await sendMessage(admin, "Whoopsie.. Something went wrong!")
		addon = ""
		if i != 1:
			addon = "s"
		await sendMessage(admin, "Message sent to {} user{}".format(i, addon))

	elif msg == "/logout" or msg == "/stop":
		if str(event.chat_id) not in database.database["users"]:
			return
		deluser(str(event.chat_id))
		updatedb()
		await event.respond("Bot stopped successfully. You have been logged out.")

	elif msg == "/status" and access(event.chat_id):
		try:
			data = await tempbase[str(event.chat_id)]["session"].get_notifications()
			strng = ""
			totlen = 0
			if len(data["Messages"]) > 0:
				totlen += len(data["Messages"])
				strng += "\n`{} `Messages".format(("   " + str(len(data["Messages"])))[-4:])
			if len(data["Grades"]) > 0:
				totlen += len(data["Grades"])
				strng += "\n`{} `Grades".format(("   " + str(len(data["Grades"])))[-4:])
			if len(data["CalendarItems"]["HomeWorks"]) > 0:
				totlen += len(data["CalendarItems"]["HomeWorks"])
				strng += "\n`{} `Exams".format(("   " + str(len(data["CalendarItems"]["HomeWorks"])))[-4:])
			if len(data["Attendances"]) > 0:
				totlen += len(data["Attendances"])
				strng += "\n`{} `Attendances".format(("   " + str(len(data["Attendances"])))[-4:])
			if len(data["CalendarItems"]["SchoolFreeDays"]) > 0:
				totlen += len(data["CalendarItems"]["SchoolFreeDays"])
				strng += "\n`{} `Free days".format(("   " + str(len(data["CalendarItems"]["SchoolFreeDays"])))[-4:])
			if len(data["CalendarItems"]["TeacherFreeDays"]) > 0:
				totlen += len(data["CalendarItems"]["TeacherFreeDays"])
				strng += "\n`{} `Teacher free days".format(("   " + str(len(data["CalendarItems"]["TeacherFreeDays"])))[-4:])
			if len(data["CalendarItems"]["ParentTeacherConferences"]) > 0:
				totlen += len(data["CalendarItems"]["ParentTeacherConferences"])
				strng += "\n`{} `Parent-teacher conferences".format(("   " + str(len(data["CalendarItems"]["ParentTeacherConferences"])))[-4:])
			if len(data["SchoolNotices"]) > 0:
				totlen += len(data["SchoolNotices"])
				strng += "\n`{} `Notices".format(("   " + str(len(data["SchoolNotices"])))[-4:])
			if strng == "":
				strng = "\n__No new events__"
			else:
				strng += "\n\n`{} `Total".format(("   " + str(totlen))[-4:])
			await event.respond("**Notification Center**\n`--------------------------------`\n{}".format(strng))
		except:
			await event.respond("**Couldn't fetch data from Synergia**\n\nHere is what you can do:\n__- Wait for session to be renewed (~5 minutes)\n- Make sure you use a valid password\n- ..or wait until Librus maiteanance break ends__")

	elif msg == "/about":
		sender = await event.get_sender()
		await bot.send_file(sender, "librusik.png", caption = "**Librusik Notifier Bot**\n[Contact admin](tg://user?id={})` | `[Visit librusik]({})\n\nExclusive bot that sends notifications from Librus Synergia about new grades, absences, messages, etc.".format(admin, database.get_conf("instance")))

	elif msg == "/start":
		sender = await event.get_sender()
		await event.respond("**Hello, {}!**\nI'm a bot that checks events on Librus Synergia and notifies my users about them. My maximum delay is 5 minutes (and I won't disturb you at night). If you are bored with continuous logging in to Synergia, start with:\n\n/configure `<login> <password>`\n\nP.S. Don't worry, your credentials are **AES-encrypted**.".format(sender.first_name))

	elif database.is_admin(event.chat_id):
		if msg.startswith("/users"):
			return await sendMessage(admin, listusers())

		elif msg.startswith("/allow"):
			if msg == "/allow":
				return await sendMessage(admin, "There is no user ID in this command...")
			user = msg[6:]
			if str(user) in database.database["users"]:
				access(user, True)
				await sendMessage(admin, "Access for [{} {}](tg://user?id={}) granted!".format(database.database["users"][str(user)]["first_name"], database.database["users"][str(user)]["last_name"], user))
				try:
					await sendMessage(int(user), "**Permission granted!**\nYou are now able to use this bot.")
				except:
					pass
			else:
				await sendMessage(admin, "User does not exist!")
			await sendMessage(admin, listusers())

		elif msg.startswith("/deny"):
			if msg == "/deny":
				return await sendMessage(admin, "There is no user ID in this command...")
			user = msg[5:]
			if str(user) in database.database["users"]:
				access(user, False)
				await sendMessage(admin, "Access for [{} {}](tg://user?id={}) denied!".format(database.database["users"][str(user)]["first_name"], database.database["users"][str(user)]["last_name"], user))
				del database.database["users"][str(user)]
				updatedb()
				await sendMessage(int(user), "**Permission denied.**\nYou have been logged out.")
			else:
				await sendMessage(admin, "User does not exist!")
			await sendMessage(admin, listusers())

bot.run_until_disconnected()
